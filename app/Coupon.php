<?php

namespace App;

use App\Filters\CouponFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class Coupon extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'link',
        'amount',
        'brand_id',
        'code',
        'type'
    ];

    public function scopeFilter(Builder $builder, $request)
    {
       if(isset($request->type))
           $builder->where('type',$request->type);
       if($request->brand_id)
           $builder->where('brand_id',$request->brand_id);
       return $builder->get();
    }
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class)->withPivot('code')->withTimestamps()->orderBy('created_at','DESC');
    }

    public function getFileAttribute($value)
    {
//        if(isset($value))
//            $value = '/'.$value;
        return $value;
    }
    public static function getCode($code,$addr)
    {
        $handle = fopen("1.txt", "r");
        $newCode = 0;
        $flag = 0;
        $expired = 0;
        if ($handle) {
            $newCode = fgets($handle);
            if(isset($code)) {
                while ($newCode !== false) {
                    $temp[] = [$newCode,$code->code];
                    if($newCode == $code->code)
                        $flag = 1;
                    $newCode = fgets($handle);
                    if($flag) {
                        $nextCode = fgets($handle);
                        if($nextCode === false)
                            $expired = 1;
                        break;
                    }
                }
            }
            fclose($handle);
            return  [$newCode,$expired];
        }
        return [0,0]; // Can't Found file
    }
}
