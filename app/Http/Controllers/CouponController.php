<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) //get all coupons can filter data with type and brand id EX: /coupons?type=discount&brand_id=1
    {
        $coupons = Coupon::filter($request);
        if($coupons)
            $coupons->makeHidden('code');
        return $coupons;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //store Data
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'amount'=>'required|max:255',
            'brand_id'=>'required|exists:brands,id', //id must be exist in brands table
            'type' => 'required|in:discount,unique,normal', //only this three item can be pass
        ]);
        $user = Coupon::create($request->all());
        return "Done";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon) //show coupon detail with code
    {
        return $coupon;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        return $coupon;

        //return view and compact coupon
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon) //update coupon
    {
        $this->validate($request, [
            'brand_id'=>'exists:brands,id', //id must be exist in brands table
            'type' => 'in:discount,unique,normal', //only this three item can be pass
        ]);
        $coupon->fill($request->all())->save();
        return "Done";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon) //delete Coupon
    {
        $coupon->delete();
        return "Done";
    }

    public function addUserCoupon(Coupon $coupon,$user) //add coupon to user
    {
        $couponData = $coupon->user()->where('user_id',$user)->first();     //check user used coupon or not
        if(!$couponData) {
            $code = DB::table('coupon_user')->select('code')->orderBy('created_at','DESC')->first();     //check last code from db
            list($newCode,$expired) = Coupon::getCode($code,$coupon->file);         //get newcode from textfile
            if($newCode) {
                 $coupon->user()->attach($user, ['code' => $newCode]);              // add coupon and code to user
                 if($expired)                   //if code finished coupon deleted ( and could be add column expire to expire coupon insist  delete coupon)
                     $this->destroy($coupon);
                return "Done!";
            }
            return "File Not Found";
        }
        return $couponData->pivot->code;
    }
}
