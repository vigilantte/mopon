<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CouponTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCoupons()
    {
        $response = $this->get('/coupons');
        $response->assertStatus(200);
    }

    public function testAddCoupon()
    {
        $this->post('/coupons',['name'=>"coupon1","amount"=>200,"link"=>"digikala.com","brand_id"=>1,"code"=>1,"type"=>"discount"])
            ->assertSeeText("Done");
    }
}
