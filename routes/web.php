<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/category','CategoryController@index'); //Get all category
Route::get('/brand','BrandController@index'); //Get all brand
Route::resource('coupons','CouponController'); // /coupons => get all coupons  can filter with type and brand_id
                                               // /coupons post data and save it

